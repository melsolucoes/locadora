
package br.cefet.locadora;

/**
 *
 * @author Julio, Matheus Flausino
 */
public abstract class  Midia {
    private int numero;
    private int censura;
    private String titulo;
    private Comum funcionario;
    private Categoria categoria; 
    
    public String toString()
    {
        return titulo ;    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    public void Midia(Categoria c){
        this.categoria = c;
    }
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getCensura() {
        return censura;
    }

    public void setCensura(int censura) {
        this.censura = censura;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Comum getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Comum funcionario) {
        this.funcionario = funcionario;
    }
    
    
}
