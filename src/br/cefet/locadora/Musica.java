
package br.cefet.locadora;

/**
 *
 * @author Julio, Matheus Flausino
 */
public class Musica extends Midia{
    private String cantor;
    private int qtdMusica;

    public String getCantor() {
        return cantor;
    }

    public void setCantor(String cantor) {
        this.cantor = cantor;
    }

    public int getQtdMusica() {
        return qtdMusica;
    }

    public void setQtdMusica(int qtdMusica) {
        this.qtdMusica = qtdMusica;
    }
    
}
