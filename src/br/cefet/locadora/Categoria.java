
package br.cefet.locadora;

/**
 *
 * @author Julio, Matheus flausino
 */
public class Categoria {
    
    private String descricao;
    private Double preco;
    private int tipo;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public String toString()
    {
        return descricao;
    }

}
