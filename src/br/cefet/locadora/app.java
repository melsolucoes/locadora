package br.cefet.locadora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alunos
 */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class app {
    
    public static void main(String[] args){
         
        Configuration cfg = new Configuration().configure();
        SchemaExport ex = new SchemaExport(cfg);
        ex.create(true, true);
     
    }
}
