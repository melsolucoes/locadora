
package br.cefet.locadora;

/**
 *
 * @author Julio, Matheus Flausino
 */
public class Jogo extends Midia{
    private String console;

    public String getConsole() {
        return console;
    }

    public void setConsole(String console) {
        this.console = console;
    }
    
}
