
package br.cefet.locadora;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Julio, Matheus Flausino
 */
public class Cliente {
    private String cpf;
    private String nome;    
    private int saldoLocacao;    
    private String telefone;
    private Set<Dependente> dependentes = new HashSet<Dependente>();
    
    @Override
    public String toString()
    {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSaldoLocacao() {
        return saldoLocacao;
    }

    public void setSaldoLocacao(int saldoLocacao) {
        this.saldoLocacao = saldoLocacao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Set<Dependente> getDependentes() {
        return dependentes;
    }

    public void setDependentes(Set<Dependente> dependentes) {
        this.dependentes = dependentes;
    }

  

    
      
}
