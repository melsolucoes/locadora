
package br.cefet.locadora;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Julio, Matheus Flausino
 */
public class Cupom {
    private int id;
    private Date dataLocacao;
    private Date dataDevolucao;
    private Cliente cliente;
    private Funcionario funcionario;
    private Double preco;
    private Dependente dependente;
    private Set<Midia> midias =  new HashSet<Midia>();

    public Set<Midia> getMidias() {
        return midias;
    }

    public void setMidias(Set<Midia> midias) {
        this.midias = midias;
    }
    
    
    
    public void Cupom(Cliente cliente,Funcionario funcionario){
        cliente = this.cliente;
        funcionario = this.funcionario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(Date dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
    
   

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Dependente getDependente() {
        return dependente;
    }

    public void setDependente(Dependente dependente) {
        this.dependente = dependente;
    }
    
    
}
