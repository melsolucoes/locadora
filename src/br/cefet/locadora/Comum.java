/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefet.locadora;

/**
 *
 * @author alunos
 */
public class Comum extends Funcionario {
    
    
    public Filme criarMidia(int numero,int censura,String titulo,String genero, String idioma){
        Filme f = new Filme();
        f.setNumero(numero);
        f.setCensura(censura);
        f.setTitulo(titulo);
        f.setGenero(genero);
        f.setIdioma(idioma);
        return f;
    }
    
    public Jogo criarMidia(int numero,int censura,String titulo,String console){
        Jogo j = new Jogo();
        j.setNumero(numero);
        j.setCensura(censura);
        j.setTitulo(titulo);
        j.setConsole(console); 
        return j;
    }
    public Musica criarMidia(int numero,int censura,String titulo,String cantor,int qtdMusica){
        Musica m = new Musica();
        m.setNumero(numero);
        m.setCensura(censura);
        m.setTitulo(titulo);
        m.setCantor(cantor);
        m.setQtdMusica(qtdMusica);        
        return m;
    }
            
}
