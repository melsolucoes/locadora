
package br.cefet.locadora;

/**
 *
 * @author Julio, Matheus flausino
 */
public class Administrador extends Funcionario{
    
    public Administrador criarFuncionarioAdministrador(String nome, String endereco,String telefone, String senha){
        Administrador a = new Administrador();
        a.setNome(nome);
        a.setEndereco(endereco);
        a.setTelefone(telefone);
        a.setSenha(senha);
        return a;
    }
    
    public Comum criarFuncionarioComum(String nome, String endereco,String telefone, String senha){
        Comum c = new Comum();
        c.setNome(nome);
        c.setEndereco(endereco);
        c.setTelefone(telefone);
        c.setSenha(senha); 
        return c;
    }
    
    
}
